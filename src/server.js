import express, { json, urlencoded } from 'express';
import { config } from 'dotenv';
import { connect, connection as _connection } from 'mongoose';
import passport from 'passport';
import weatherController from '@controllers/wather'
var cookieSession = require('cookie-session');
config();
require('passport-setup.js');

// import routes from './routes';
// const path = require('path');

//Import Routes
// const authRoute = require('./routes/auth').default;
// const channelsRoute = require('./routes/channels')
//connect to DB
connect(process.env.DB_CONNECT, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify:false, useCreateIndex: true});
const app = express()
const connection = _connection;

connection.once("open", function() {
  console.log("database connection initialized")
  // logger.log("info","MongoDB database connection established successfully");
});
//Middleware
app.use(json())
app.use(cookieSession({
  name: 'weather-api',
  keys: ['key1', 'key2']
}))
const isLoggedIn = (req, res, next) => {
  if(req.user){
    next()
  } else {
    res.status(401).send("Unathorized");
  }
}
app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => res.send("You are logout"));


app.get('/weather-now', isLoggedIn, weatherController.getWeatherByCoordinates);
app.post('/check-adress', weatherController.checkAdress);
app.get('/failed', (req, res) => res.send("Login failed"));
app.get('/loginSuccess', isLoggedIn, (req, res) => res.send(`Login done, welcome ${req.user.displayName} `));







app.get('/google',
  passport.authenticate('google', { scope:
      [ 'email', 'profile' ] }
));

app.get( '/google/callback',
    passport.authenticate( 'google', {
        successRedirect: '/loginSuccess',
        failureRedirect: '/failed'
}));

app.use('/logout', (req, res) => {
  req.session = null;
  req.logout();
  res.redirect('/')
});

//Router middlewares
// app.use('/api/user', authRoute);
// app.use('/api/channels', channelsRoute)
const port = process.env.PORT || 3000;
app.listen(port,()=> console.log(`Server Started on port ${port}`))