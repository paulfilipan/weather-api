import https from 'https';
import temperatureRepo from '@repository/temperatureRepo';
var fahrenheitToCelsius = require('fahrenheit-to-celsius');

const checkWeather = async (lat, lon) => {
    const callApi = async (lat, lon) => {
        return new Promise((resolve, reject) => {
            https.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${process.env.API_WATHER}`, (resp) => {
                let data = '';

                // A chunk of data has been received.
                resp.on('data', (chunk) => {
                    console.log(data)
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    // console.log(data)
                    resolve(JSON.parse(data));
                });

            }).on("error", (err) => {
                reject("Error: " + err.message);
            });

        })
    }
    const temperatureExist = await temperatureRepo.retrieveTemperatureByLanLon(lat, lon);
    if (temperatureExist) {
        return temperatureExist;
    } else {
        const data = await callApi(lat, lon);
        const { main, sys, id } = data;
        const createdOrUpdatedTemperature = temperatureRepo.createOrUpdate({
            lat,
            lon,
            temp: fahrenheitToCelsius(main.temp),
            country: sys.country,
            id
        })
        return createdOrUpdatedTemperature;
    }
}

const checkAddress = async (address) => {
    const callApi = async (adress) => {
        return new Promise((resolve, reject) => {
            var API_KEY = process.env.GOOGLE_API;
            var BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=";

            var address = "1600 Amphitheatre Parkway, Mountain View, CA";

            var url = BASE_URL + address + "&key=" + API_KEY;

            https.get(url, (resp) => {
                let data = '';

                // A chunk of data has been received.
                resp.on('data', (chunk) => {
                    console.log(data)
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    console.log(data)
                    resolve(JSON.parse(data));
                });

            }).on("error", (err) => {
                reject("Error: " + err.message);
            });

        })
    }
    const addressExist = await callApi(address);

    if (addressExist) {
        return true;
    } else {
        return false;
    }
}

export default {
    checkWeather,
    checkAddress,
}