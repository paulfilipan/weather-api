const Joi = require('@hapi/joi');

const addressValidation = (data) => {
    const schema = Joi.object({
        street: Joi.string().min(1).required(),
        streetNumber: Joi.number().required(),
        town: Joi.string().min(1).required(),
        postalCode: Joi.number().required(),
        country: Joi.string().min(1).required()
    });
    
    return schema.validate(data);
}

export default {
    addressValidation,
}