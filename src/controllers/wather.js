import service from '@services/checkWeather'
import validations from '../utils/validation';

const getWeatherByCoordinates = async (req, res) => {
    const { lat, long } = req.query;
    const data = await service.checkWeather(lat,long);
    const { temp, country } = data;
    res.status(200).send(`there are ${temp} degrees there in ${country}`)
};

const checkAdress = async (req, res) => {
    const { street, streetNumber, town, postalCode, country } = req.body;
    const { error } = validations.addressValidation({ street, streetNumber, town, postalCode, country });
    if (error) {
        return res.status(400).json({ error: error.details[0].message })
    }
    const validAddress = await service.checkAddress({ street, streetNumber, town, postalCode, country });
    if(validAddress) {
        res.status(200).send(`Address ${street}, ${streetNumber}, ${town}, ${postalCode}, ${country}, is valid`)
    } else {
        res.status(400).send(`Address ${street}, ${streetNumber}, ${town}, ${postalCode}, ${country}, is not valid`)
    }
};

export default {
    getWeatherByCoordinates,
    checkAdress
}