import Temperature from '../model/Temperature';

const createOrUpdate = async (input) => {
    const { id } = input;
    const temperature = await Temperature.findOne({ id });
    if (temperature) {
        console.log("update")
        const temperatureUpdated = await Temperature.findOneAndUpdate({ id },{temp: input.temp, lastChecked: Date.now()},{new: true});
        return temperatureUpdated;
    } else {
        console.log("create")

        const temperature = new Temperature({
            lat: input.lat,
            lon: input.lon,
            country: input.country,
            temp: input.temp,
            id: input.id,
        });
        const savedTemperature = await temperature.save();
        return savedTemperature;
    }
};

const retrieveTemperatureByLanLon = async (lat, lon) => {
    const temperature = await Temperature.findOne({ 
        lat,
        lon,
        lastChecked: { $gt: new Date(Date.now() - 43200000) }
    });
    console.log("get from db", temperature)
    return temperature;
};

export default {
    createOrUpdate,
    retrieveTemperatureByLanLon,
};