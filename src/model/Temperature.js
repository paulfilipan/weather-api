import { Schema, model } from 'mongoose';

const temperature = new Schema({
    lastChecked: {
        type: Date,
        default: Date.now
    },
    lat: {
        type: String,
        required:true,
    },
    lon: {
        type: String,
        required:true,
    },
    country: {
        type: String,
        required:true,
    },
    temp: {
        type: Number,
        required:true,
    },
    id: {
        type: Number,
        required:true,
    },
})

export default model('Temperature', temperature)